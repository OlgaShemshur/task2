/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.comparator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import taxistation.builder.Transport;

/**
 *
 * @author Olga
 */
public class SortByFuelConsumptionTest {
    
    public SortByFuelConsumptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compare method, of class SortByFuelConsumption.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        Transport ob1 = new Transport("name", 200, 1, 5, 2000, "mark");
        Transport ob2 = new Transport("name", 200, 1, 5, 2000, "mark");
        SortByFuelConsumption instance = new SortByFuelConsumption();
        int expResult = 0;
        int result = instance.compare(ob1, ob2);
        assertEquals(expResult, result);
        Transport ob3 = new Transport("name", 200, 1, 5.2, 2000, "mark");
        expResult = -1;
        result = instance.compare(ob1, ob3);
        assertEquals(expResult, result);
    }
    
}
