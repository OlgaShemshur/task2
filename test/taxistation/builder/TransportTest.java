/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder;

import org.junit.Test;
import static org.junit.Assert.*;
import taxistation.builder.products.Bus;

/**
 *
 * @author Olga
 */
public class TransportTest {
    
    public TransportTest() {
    }
    
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        int expResult = 2000;
        int result = instance.getPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFuelConsumption method, of class Transport.
     */
    @Test
    public void testGetFuelConsumption() {
        System.out.println("getFuelConsumption");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        double expResult = 5;
        double result = instance.getFuelConsumption();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class Transport.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        String expResult = "name\n______________\nКоличество пассажиров: 1\nРасход топлива: 5.0\nГрузоподъемность: 200\nМарка: mark\nЦена: 2000\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of read method, of class Transport.
     */
    @Test
    public void testRead() {
        System.out.println("read");
        int i1 = 0;
        String fileName = "src\\resource\\inputTaxi.txt";
        Transport instance = new Transport();
        String expResult = "1860";
        String result = instance.read(i1, fileName);
        assertEquals(expResult, result);
        i1 = 1;
        expResult = "9.1";
        result = instance.read(i1, fileName);
        assertEquals(expResult, result);
        i1 = 2;
        expResult = "4";
        result = instance.read(i1, fileName);
        assertEquals(expResult, result);
        i1 = 3;
        expResult = "550";
        result = instance.read(i1, fileName);
        assertEquals(expResult, result);
        i1 = 4;
        expResult = "Audi";
        result = instance.read(i1, fileName);
        assertEquals(expResult, result);
        i1 = 5;
        expResult = "true";
        result = instance.read(i1, fileName);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Transport.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        TransportBuilder tb = new Bus();
        tb.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        boolean expResult = true;
        boolean result = instance.equals(tb.getTransport());
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Transport.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        int expResult = 12665;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of print method, of class Transport.
     */
    @Test
    public void testPrint() {
        System.out.println("print");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        String expResult = "name\n______________\nКоличество пассажиров: 1\nРасход топлива: 5.0\nГрузоподъемность: 200\nМарка: mark\nЦена: 2000\n";
        String result = instance.print();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPrise method, of class Transport.
     */
    @Test
    public void testGetPrise() {
        System.out.println("getPrise");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        int expResult = 2000;
        int result = instance.getPrise();
        assertEquals(expResult, result);
    }
    
}
