/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder.products;

import org.junit.Test;
import static org.junit.Assert.*;
import taxistation.builder.Transport;
import taxistation.builder.TransportBuilder;

/**
 *
 * @author Olga
 */
public class TaxiTest {
    
    /**
     * Test of getIsDriverSing method, of class Taxi.
     */
    @Test
    public void testGetIsDriverSing() {
        System.out.println("getIsDriverSing");
        Taxi instance = new Taxi();
        boolean expResult = false;
        boolean result = instance.getIsDriverSing();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Taxi.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TransportBuilder instance = new Taxi();
        instance.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        String expResult = "name\n______________\nКоличество пассажиров: 1\nРасход топлива: 5.0\nГрузоподъемность: 200\nМарка: mark\nЦена: 2000\nПоет ли водитель : нет\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of print method, of class Taxi.
     */
    @Test
    public void testPrint() {
        System.out.println("print");
        TransportBuilder instance = new Taxi();
        instance.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        String expResult = "name\n______________\nКоличество пассажиров: 1\nРасход топлива: 5.0\nГрузоподъемность: 200\nМарка: mark\nЦена: 2000\nПоет ли водитель : нет\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPrise method, of class Taxi.
     */
    @Test
    public void testGetPrise() {
        System.out.println("getPrise");
        Taxi instance = new Taxi();
        int expResult = 0;
        int result = instance.getPrise();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Taxi.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        TransportBuilder tb = new Taxi();
        tb.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
        boolean expResult = true;
        boolean result = instance.equals(tb.getTransport());
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hashCode method, of class MiniBus.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        TransportBuilder instance = new Taxi();
        instance.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        int expResult = 12664;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
}
