/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder.products;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import taxistation.builder.Transport;
import taxistation.builder.TransportBuilder;

/**
 *
 * @author Olga
 */
public class MiniBusTest {
    
    public MiniBusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getStationsLenth method, of class MiniBus.
     */
    @Test
    public void testGetStationsLenth() {
        System.out.println("getStationsLenth");
        MiniBus instance = new MiniBus();
        int expResult = 0;
        int result = instance.getStationsLenth();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFuelConsumption method, of class MiniBus.
     */
    @Test
    public void testGetFuelConsumption() {
        TransportBuilder instance = new MiniBus();
        instance.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        double expResult = 5.0;
        double result = instance.getFuelConsumption();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class MiniBus.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TransportBuilder instance = new MiniBus();
        instance.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        String expResult = "name\n______________\nКоличество пассажиров: 1\nРасход топлива: 5.0\nГрузоподъемность: 200\nМарка: mark\nЦена: 2000\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of print method, of class MiniBus.
     */
    @Test
    public void testPrint() {
        System.out.println("print");
        TransportBuilder instance = new MiniBus();
        instance.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        String expResult = "name\n______________\nКоличество пассажиров: 1\nРасход топлива: 5.0\nГрузоподъемность: 200\nМарка: mark\nЦена: 2000\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPrise method, of class MiniBus.
     */
    @Test
    public void testGetPrise() {
        System.out.println("getPrise");
        MiniBus instance = new MiniBus();
        int expResult = 0;
        int result = instance.getPrise();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class MiniBus.
     */
    @Test
    public void testEquals() {
       System.out.println("equals");
       TransportBuilder tb = new MiniBus();
       tb.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
       Transport instance = new Transport("name", 200, 1, 5, 2000, "mark");
       boolean expResult = true;
       boolean result = instance.equals(tb.getTransport());
       assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class MiniBus.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        TransportBuilder instance = new MiniBus();
        instance.createNewTransportProduct("name", 200, 1, 5, 2000, "mark");
        int expResult = 12665;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }    
}
