/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.comparator;

import java.util.Comparator;
import taxistation.controller.Print;

/**
 *
 * @author Olga
 */
public class SortByFuelConsumption implements Comparator {

    @Override
    public int compare(Object ob1, Object ob2) {
        Print o1 = (Print)ob1;
        Print o2 = (Print)ob2;
        double fuelConsumption1 = o1.getFuelConsumption();
             double fuelConsumption2 = o2.getFuelConsumption();
             if(fuelConsumption1 > fuelConsumption2) {
                    return 1;
             }
             else if(fuelConsumption1 < fuelConsumption2) {
                    return -1;
             }
             else {
                    return 0;
             }
    }
    
}
