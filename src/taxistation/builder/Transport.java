/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder;

import taxistation.controller.Print;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Olga
 */
public class Transport implements Print {
    static{
        new DOMConfigurator().doConfigure("src\\resource\\log4jTransport.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Transport.class.getName());
    
    private String name;
    private int duty;
    private int numberOfPassengers;
    private double fuelConsumption;
    private int price;
    private String mark;

    public void setDuty(int duty)     { this.duty = duty; }
    public void setNumberOfPassengers(int numberOfPassengers)     { this.numberOfPassengers = numberOfPassengers; }
    public void setFuelConsumption(double fuelConsumption)   { this.fuelConsumption = fuelConsumption; }
    public void setPrice(int price)     { this.price = price; }
    public void setMark(String mark)     { this.mark = mark; }
    public void setName(String name)     { this.name = name; }
    public int getPrice()     { return price; }
    @Override
    public double getFuelConsumption()     { return fuelConsumption; }
    @Override
    public int getDuty()     { return duty; }
    @Override
    public int getNumberOfPassengers()     { return numberOfPassengers; }
    public String getMark()     { return mark; }
    public String getName()     { return name; }
    
    public Transport() {
     name = "";
     duty = 0;
     numberOfPassengers = 0;
     fuelConsumption = 0;
     price = 0;
     mark = "";
    }
    
     public Transport(String name,int duty, int numberOfPassengers, double fuelConsumption, int price, String mark) {
     this.name = name;
     this.duty = duty;
     this.numberOfPassengers = numberOfPassengers;
     this.fuelConsumption = fuelConsumption;
     this.price = price;
     this.mark = mark;
    }
     
   @Override 
    public String toString()  {
        return name + "\n______________\nPassenger quantity: " + numberOfPassengers + "\nFuel consumption: " + fuelConsumption + "\nDuty: " + duty + "\nMark: " + mark + "\nPrice: " + price + "\n";
    }
    
    public String read(int i1, String fileName) { 
    String s = "";
    try(FileReader reader = new FileReader(fileName))
        {
            int c;
            int i = 0;
            while((c=reader.read())!=-1){ 
                char k = (char)c;
                if(k==' ') {
                    if(i==i1) {
                    break;
                }
                else {
                   s = "";
                   i++;
                   continue;                        
                }                    
                }
                s += k;
            } 
        }
        catch(IOException ex){
           logger.error("Input error: ", ex);
        } 
    return s;
    }
    
    @Override
    public boolean equals(Object o)  {
      if (o == null)
          return false;
      if (this == o)
          return true;
      if (getClass() != o.getClass())
	return false;
      Transport tmp = (Transport)o;
      return tmp.duty == duty && tmp.numberOfPassengers == numberOfPassengers && tmp.fuelConsumption == fuelConsumption && tmp.price == price && tmp.mark.equals(mark);
    }
    
    @Override
    public int hashCode()  {
        Double ob = fuelConsumption;
        int i =  name.length()*2 + duty*3 + numberOfPassengers*4 + ob.intValue()*5 + price*6 + mark.length()*7;
        return i;
    }

    @Override
    public String print() {
        return name + "\n______________\nPassenger quantity: " + numberOfPassengers + "\nFuel consumption: " + fuelConsumption + "\nDuty: " + duty + "\nMark: " + mark + "\nPrice: " + price + "\n";
    }

    @Override
    public int getPrise() {
        return price;
    }
}
