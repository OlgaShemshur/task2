/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder.products;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import taxistation.builder.Transport;
import taxistation.builder.TransportBuilder;
import taxistation.controller.Print;

/**
 *
 * @author Olga
 */
public class CargoTaxi extends TransportBuilder implements Print {

    static{
        new DOMConfigurator().doConfigure("src\\resource\\log4jTransport.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Transport.class.getName());
    
    private int square;
    
    @Override
    public double getFuelConsumption()     { return transport.getFuelConsumption(); }
    @Override
    public int getDuty()     { return transport.getDuty(); }
    @Override
    public int getNumberOfPassengers()     { return transport.getNumberOfPassengers(); }
    public String getMark()     { return transport.getMark(); }
    public String getName()     { return transport.getName(); }
    
    @Override
    public void buildDuty() {
        String s = transport.read(0, "src\\resource\\inputCargoTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputCargoTaxi\"");
            return;
        }
        transport.setDuty(Integer.parseInt(s));
    }

    @Override
    public int buildPrice() {
        String s = transport.read(3, "src\\resource\\inputCargoTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputCargoTaxi\"");
            return 0;
        }
        transport.setPrice(Integer.parseInt(s));
        return Integer.parseInt(s);
    }

    @Override
    public void buildNumberOfPassengers() {
        String s = transport.read(2, "src\\resource\\inputCargoTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputCargoTaxi\"");
            return;
        }
        transport.setNumberOfPassengers(Integer.parseInt(s));
    }

    @Override
    public void buildFuelConsumption() {
        String s = transport.read(1, "src\\resource\\inputCargoTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputCargoTaxi\"");
            return;
        }
        transport.setFuelConsumption(Double.parseDouble(s));
    }

    @Override
    public void buildOther() {
        String s = transport.read(5, "src\\resource\\inputCargoTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputCargoTaxi\"");
            return;
        }
        square = Integer.parseInt(s);
    }

    @Override
    public void buildMark() {
        String s = transport.read(4, "src\\resource\\inputCargoTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputCargoTaxi\"");
            return;
        }
        transport.setMark(s);
    }
    
    @Override
    public String toString() {
        String s = transport.toString();
        s += "Square: " + square + "\n";
        return s;
    }
    
    @Override
    public void buildName() {
        transport.setName("Cargo taxi");
    }

    @Override
    public String print() {
        String s = transport.toString();
        s += "Square: " + square + " m\n";
        return s;
    }

    @Override
    public int getPrise() {
         if(transport!= null) return transport.getPrice();
         else return 0;
    }
    
    @Override
    public int hashCode()  {
        Double ob = getFuelConsumption();
        int i =  getName().length()*2 + getDuty()*3 + getNumberOfPassengers()*4 + ob.intValue()*5 + getPrise()*6 + getMark().length()*7;
        return  i  + square;
    }

    @Override
    public boolean equals(Object o)  {
      if (null == o)
          return false;
      if (this == o)
          return true;
      if (getClass() != o.getClass())
	return false;
      CargoTaxi tmp = (CargoTaxi)o;
      return tmp.getDuty() == getDuty() && tmp.getNumberOfPassengers() == getNumberOfPassengers() && tmp.getFuelConsumption() == getFuelConsumption() && tmp.getPrise() == getPrise() && tmp.getMark().equals(getMark()) && square == tmp.square;
    }
}
