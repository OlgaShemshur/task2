/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder.products;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import taxistation.builder.Transport;
import taxistation.controller.Print;
import taxistation.builder.TransportBuilder;

/**
 *
 * @author Olga
 */
public class Taxi extends TransportBuilder implements Print {

    static{
        new DOMConfigurator().doConfigure("src\\resource\\log4jTransport.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Transport.class.getName());
    
    private boolean isDriverSing;
    
    boolean getIsDriverSing() { return isDriverSing; }
    @Override
    public double getFuelConsumption()     { return transport.getFuelConsumption(); }
    @Override
    public int getDuty()     { return transport.getDuty(); }
    @Override
    public int getNumberOfPassengers()     { return transport.getNumberOfPassengers(); }
    public String getMark()     { return transport.getMark(); }
    public String getName()     { return transport.getName(); }
    
    
    @Override
    public void buildDuty() {
        String s = transport.read(0, "src\\resource\\inputTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputTaxi\"");
            return;
        }
        transport.setDuty(Integer.parseInt(s));
    }

    @Override
    public int buildPrice() {
        String s = transport.read(3, "src\\resource\\inputTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputTaxi\"");
            return 0;
        }
        transport.setPrice(Integer.parseInt(s));
        return Integer.parseInt(s);
    }

    @Override
    public void buildNumberOfPassengers() {
        String s = transport.read(2, "src\\resource\\inputTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputTaxi\"");
            return;
        }
        transport.setNumberOfPassengers(Integer.parseInt(s));
    }

    @Override
    public void buildFuelConsumption() {
        String s = transport.read(1, "src\\resource\\inputTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputTaxi\"");
            return;
        }
        transport.setFuelConsumption(Double.parseDouble(s));
    }

    @Override
    public void buildOther() {
        String s = transport.read(5, "src\\resource\\inputTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputTaxi\"");
            return;
        }
        isDriverSing = new Boolean(s);
    }

    @Override
    public void buildMark() {
        String s = transport.read(4, "src\\resource\\inputTaxi.txt");
        if(s.equals("")) {
            logger.error("Read file error\"inputTaxi\"");
            return;
        }
        transport.setMark(s);
    }

    @Override
    public String toString() {
        String s = transport.toString();
        s += "Is driver sing : ";
        if(isDriverSing) s +="yes\n";
        else s +="no\n";
        return s;
    }

    @Override
    public void buildName() {
        transport.setName("Taxi");
    }

    @Override
    public String print() {
        String s = transport.toString();
        s += "Is driver sing : ";
        if(isDriverSing) s +="yes\n";
        else s +="no\n";
        return s;
    }
    
    @Override
    public int getPrise() {
        if(transport!= null) return transport.getPrice();
        else return 0;
    }
    
    @Override
    public boolean equals(Object o)  {
      if (null == o)
          return false;
      if (this == o)
          return true;
      if (getClass() != o.getClass())
	return false;
      Taxi tmp = (Taxi)o;
      return tmp.getDuty() == getDuty() && tmp.getNumberOfPassengers() == getNumberOfPassengers() && tmp.getFuelConsumption() == getFuelConsumption() && tmp.getPrise() == getPrise() && tmp.getMark().equals(getMark()) && isDriverSing == tmp.getIsDriverSing();
    }
    
    @Override
    public int hashCode()  {
        Double ob = getFuelConsumption();
        int i =  getName().length()*2 + getDuty()*3 + getNumberOfPassengers()*4 + ob.intValue()*5 + getPrise()*6 + getMark().length()*7;
        if(isDriverSing) return  i + 1;
        return  i - 1;
    }
}
