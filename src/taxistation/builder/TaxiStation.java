/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder;

import taxistation.controller.Print;
import java.util.ArrayList;
import java.util.Collections;
import taxistation.comparator.SortByFuelConsumption;

/**
 *
 * @author Olga
 */
public class TaxiStation {
    private ArrayList<Print> allTransport = new ArrayList<>();
    private TransportBuilder transportBuilder;
    private int price;
    private String name;
    
    public void setTransportBuilder(TransportBuilder ob) { 
        transportBuilder = ob;
    }
    
    public Transport getTransport() { 
        if(transportBuilder != null)  return transportBuilder.getTransport();
        else return null;
    }
    
    public Print getPrint() { 
        if(transportBuilder != null && transportBuilder.getTransport() != null) return (Print)transportBuilder.getTransport();
        else return null;
    }

    public int getArrayLenth() { return allTransport.size(); }
    
    public TaxiStation(){
        price = 0;
        name = null;
    }
    
    public TaxiStation(String name){
        price = 0;
        this.name = name;
    }
    
    public void constructTransport() {
       transportBuilder.createNewTransportProduct();
       transportBuilder.buildDuty();
       transportBuilder.buildFuelConsumption();
       transportBuilder.buildNumberOfPassengers();
       price+=transportBuilder.buildPrice();
       transportBuilder.buildMark();
       transportBuilder.buildOther();
       transportBuilder.buildName();
    }

    @Override
    public String toString() {
        String s = "";
        if(name!=null)s += "\"" + name + "\"\n";
        s += "Taxistation price" + ":" + price + "\nAvailible transport:\n";
        int iterator = 1;
        for(Print elem: allTransport) {
            s += iterator++ + ") " + elem.print() + "\n";
        }
        return s;
    } 
    
    public void add(Print ob) {
        allTransport.add(ob);
        if(ob!=null) price += ob.getPrise();
        sort();
    }
    
    public void sort() {
        Collections.sort(allTransport, new SortByFuelConsumption());
    }
    
    public ArrayList<Print> showInterval(int dutyLeftBorder, int dutyRightBorder, int numberOfPassengersLeftBorder, int numberOfPassengersRightBorder) {
        ArrayList<Print> transport = new ArrayList<>();
        for(Print elem: allTransport) {
            if((elem.getDuty() >= dutyLeftBorder && elem.getDuty() <= dutyRightBorder) || (elem.getDuty() >= dutyRightBorder && elem.getDuty() <= dutyLeftBorder)) {
                if((elem.getNumberOfPassengers() >= numberOfPassengersLeftBorder && elem.getNumberOfPassengers() <= numberOfPassengersRightBorder) || (elem.getNumberOfPassengers() >= numberOfPassengersRightBorder && elem.getNumberOfPassengers() <= numberOfPassengersLeftBorder)) {                    
                transport.add(elem);
                }
        }
    }
        return transport;
    }
}
