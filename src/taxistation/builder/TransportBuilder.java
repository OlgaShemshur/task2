/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.builder;

/**
 *
 * @author Olga
 */
public abstract class TransportBuilder {
    protected Transport transport;
    
    public Transport getTransport() { return transport; }
    public void createNewTransportProduct() { transport = new Transport(); }
    public void createNewTransportProduct(String name,int duty, int numberOfPassengers, double fuelConsumption, int price, String mark) {
        transport = new Transport(name, duty, numberOfPassengers, fuelConsumption, price, mark); 
    }

    public abstract double getFuelConsumption();
    public abstract void buildDuty();
    public abstract void buildNumberOfPassengers();
    public abstract void buildFuelConsumption();
    public abstract int buildPrice();
    public abstract void buildMark();
    public abstract void buildOther();
    public abstract void buildName();
}
