/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation;

import taxistation.builder.TransportBuilder;
import taxistation.builder.TaxiStation;
import taxistation.controller.Print;
import taxistation.builder.products.CargoTaxi;
import taxistation.builder.products.Taxi;
import taxistation.builder.products.MiniBus;
import taxistation.builder.products.Bus;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
/**
 *
 * @author Olga
 */
public class Main {
    
    static{
        new DOMConfigurator().doConfigure("src\\resource\\log4jMain.xml", LogManager.getLoggerRepository());
    }
    static Logger logger = Logger.getLogger(Main.class.getName());
    static Logger loggerOut = Logger.getLogger(Print.class.getName());
    
    public static void main(String[] args) {
        TaxiStation taxistation = new TaxiStation("Taxistation №1");
        TransportBuilder transportBuilder = null;
        int [] arr = new int[4];
        try(FileReader reader = new FileReader("src\\resource\\inputQuantity.txt"))
        {
            int c;
            int i = 0;
            String s = "";
            while((c=reader.read())!=-1){ 
                if(i>3) throw new IOException();
                char k = (char)c;
                if(k==' ') {
                    try{
                        arr[i] = Integer.parseInt(s);
                    }
                    catch(Exception ex){
                        logger.error("Input error. Not integer value was input: ", ex);
                        return;
                    } 
                    s = "";
                    i++;
                    continue;
                }
                s += ""+ k;
            } 
            arr[i] = Integer.parseInt(s);
        }
        catch(IOException ex){
           logger.error("Input error: ", ex);
           return;
        } 
        for(int i=0; i<4; i++)
        for(int y=0; y<arr[i]; y++) {
            if(i==0) transportBuilder = new Bus();
            else if(i==1) transportBuilder = new Taxi();
            else if(i==2) transportBuilder = new MiniBus();
            else transportBuilder = new CargoTaxi();
            taxistation.setTransportBuilder(transportBuilder);
            taxistation.constructTransport();
            taxistation.add((Print)transportBuilder);
        }
        int [] diaposon = new int[4];
        System.out.println(taxistation);
        try(FileReader reader = new FileReader("src\\resource\\inputInterval.txt"))
        {
            int c;
            int i = 0;
            String s = "";
            while((c=reader.read())!=-1){ 
                if(i>3) throw new IOException();
                char k = (char)c;
                if(k==' ') {
                    try{
                        diaposon[i] = Integer.parseInt(s);
                    }
                    catch(Exception ex){
                        logger.error("Input error. Not integer value was input: ", ex);
                        return;
                    } 
                    s = "";
                    i++;
                    continue;
                }
                s += ""+ k;
            } 
            diaposon[i] = Integer.parseInt(s);
        }
        catch(IOException ex){
           logger.error("Input error: ", ex);
           return;
        } 
        ArrayList<Print> transport = new ArrayList<>();
        transport = taxistation.showInterval(diaposon[0], diaposon[1], diaposon[2], diaposon[3]);
        loggerOut.info("__________________________\nChoosen transport\n");
        ArrayList<Print> typeTransport = new ArrayList<>();
        boolean firstTime = true;
        for(Print elem: transport) {
            if(firstTime) {
                typeTransport.add(elem);
                loggerOut.info(elem);
                firstTime = false;
            }                    
            for(Print elem1: typeTransport) {
                 if (!elem1.equals(elem)) {
                    typeTransport.add(elem);
                    loggerOut.info(elem);
                 }
            }
        }
    }
}
/*
<log4j:configuration>

<appender name="FILE" class="org.apache.log4j.FileAppender">

   <param name="file" value="logs/main.txt"/>
   <param name="immediateFlush" value="true"/>
   <param name="threshold" value="debug"/>
   <param name="append" value="true"/>
    <layout class="org.apache.log4j.PatternLayout">
         <param name="ConversionPattern" value="%d{DATE} - %-5p [%c] - %m%n"/>
    </layout>
</appender>

<logger name="taxistation.Main" additivity="false">
   <level value="INFO"/>
   <appender-ref ref="FILE"/>
</logger>

</log4j:configuration>



*/