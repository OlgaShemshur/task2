/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taxistation.controller;

/**
 *
 * @author Olga
 */
public interface Print {
    public String print();
    public int getPrise();
    public double getFuelConsumption();
    public int getDuty();
    public int getNumberOfPassengers();
}
